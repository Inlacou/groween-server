var mkdirp = require('mkdirp'),
	maxMultiplicator = 4;

//TODO esta todo con PNG, hacerlo variable

function resizeImageToBiggest(dir, name, extension, sizes, position, finish){
	console.log("gm.controller.js.resizeImageToBiggest");
	var size = sizes[position];
	console.log(size)

	// obtain an image object:
	require('lwip').open(dir+name+"."+extension, function(err, image, done){
		if(err){
			//TODO
      		console.log(err);
		}

		var newDir = dir+size.width+"x"+size.height+"/";

		mkdirp(newDir, function(err, made){
			if(err){
        		console.log(err);
			}

			var newFullName = newDir+"reference"+"."+extension

			//Define el maximo posible obtenible de anchura
			var maxWidth = image.width();
			//Define el maximo posible obtenible de altura
			var maxHeight = image.height();
			var finalWidth = 0;
			var finalHeight = 0;

			//Sacamos los tamaños maximos que se necesitan (aunque luego no se puedan conseguir)
			if(size.width*maxMultiplicator>finalWidth) {
				finalWidth=size.width*maxMultiplicator;
			}
			if(size.height*maxMultiplicator>finalHeight) {
				finalHeight=size.height*maxMultiplicator;
			}

			/*//Si necesitamos obtener una altura o anchura mayor de la maxima...
			if(finalWidth>maxWidth || finalHeight>maxHeight){
				if(finalWidth>finalHeight){
					finalHeight = finalHeight*maxWidth/finalWidth; //Primero calculamos el otro parametro que le acompañaria manteniendo el aspect ratio
					finalWidth = maxWidth; //y despues la equiparamos a la maxima
				}else{
					finalWidth = finalWidth*maxHeight/finalHeight;
					finalHeight = maxHeight;
				}
			}*/

			//Obtenemos los dos posibles escalados (anchura o altura)
			var scaleWidth=finalWidth/image.width();
			var scaleHeight=finalHeight/image.height();

			var finalScale;
			//Determinamos el escalado que menos informacion nos lleve a perder
			if(scaleWidth>scaleHeight){
				finalScale = scaleWidth;
			}else{
				finalScale = scaleHeight;
			}
			//Necesitamos un solo valor para el escalador para que no se deforme la imagen

			console.log("About to batch");
			console.log("Scale: " + finalScale);
			console.log("Crop: " + finalWidth + " | " + finalHeight);

			// check err...
			// define a batch of manipulations and save to disk as JPEG:
			image
			.batch()
			.scale(finalScale) 					//Escalamos
			.crop(finalWidth, finalHeight)      // cropeamos (salvo error mio, aqui ya solo haria crop de verdad de altura o anchura, vamos que solo cortaria en un sentido)
		    /*.toBuffer('png', function(err, buffer){
		    	console.log("Buffered");
		    	console.log(buffer.toString('base64'))
		    });*/
		    .writeFile(newFullName, function(err){ //Guardamos la imagen
		      // check err...
		      // done.
		      position = position+1;
		      console.log("position: " + position + " | sizes.length: " + sizes.length)
		      if(position<sizes.length){
			      resizeImageToBiggest(dir, name, extension, sizes, position, function (err){
		  			return finish(false);
		  		  });
		  	  }else{
		  	  	return finish(false);
		  	  }
		    });
		});
	});
}

exports.getImageResized = function(fullName, extension, width, height, multiplier, done){
	console.log("gm.controller.js.getImageResized");
	console.log(fullName + " " + width + " " + height + " " + multiplier);
	// obtain an image object:

	require('lwip').open(fullName, function(err, image) {
    if (err) {
      console.log(err);
      done(true);
      return;
    }

		var scaleWidth;
		var scaleHeight;

		if (width && height){
			width = width*multiplier;
			height = height*multiplier;
			console.log("Width:  tengo: "+image.width() +  " necesito: " + width);
			console.log("Height: tengo: "+image.height() + " necesito: " + height);

			scaleWidth=width/image.width();
			scaleHeight=height/image.height();

			var finalScale;
			if(scaleWidth<scaleHeight){
				finalScale = scaleWidth;
			}else{
				finalScale = scaleHeight;
			}
		}else{
			finalScale = multiplier;
		}

		console.log("scale: " + finalScale);

	    // check err...
	    // define a batch of manipulations and save to disk as JPEG:
	    image.batch()
		    .scale(finalScale)
		    .toBuffer(extension, function(err, buffer){
			   	console.log("SUCCESS Buffered");
			  	//done(false, extension, buffer.toString('base64'));
			  	var fs = require('fs');
				var stream = fs.createWriteStream(fullName+"edited"+finalScale+"."+extension);
				stream.once('open', function(fd) {
				  stream.write(buffer);
				  stream.end();
				});
			  	done(false, extension, buffer);
			});
			/*.writeFile(fullName+"edited"+finalScale+"."+extension, function(err){ //Guardamos la imagen
			    // check err...
			    // done.
			});*/
	});
}

exports.saveImage = function(dir, fileName, fileExtension, base64Data, sizes, done) {
	console.log("gm.controller.js.saveImage");
	console.log(sizes)

	fileName = "original"

	var fullName = dir+fileName+"."+fileExtension;

	console.log("Trying to save image on " + fullName);
	//Primero guardamos la imagen
	require("fs").writeFile(fullName, base64Data, 'base64', function(err) {
	  if(err){
	  	console.log("ERROR saving image")
	  	done(true, err);
	  }else{
	  	console.log("SUCCESS saving image")
	  	//Ahora que se ha guardado, podemos manipularla
	  	/*for (var i = sizes.length - 1; i >= 0; i--) {
	  		resizeImageToBiggest(dir, fileName, fileExtension, sizes[i]);
	  	};*/
	  	resizeImageToBiggest(dir, fileName, fileExtension, sizes, 0, function (err){
	  		return done(false, dir, fileName, fileExtension);
	  	});
	  	//Y dar respuesta de que todo ha ido bien
	  }
	});
};
