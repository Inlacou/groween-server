/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

"use strict";

//var Thing = require("../api/thing/thing.model");
var mongoose = require("mongoose");
var SubscriptionType = require("../api/subscriptionType/subscriptionType.model");
var User = require("../api/user/user.model");
var Area = require("../api/area/area.model");
var Question = require("../api/question/question.model");
var Template = require("../api/template/template.model");
var Category = mongoose.model("Category");
var Topic = mongoose.model("Topic");
var Profession = require("../api/profession/profession.model");
var prefix = "SEED.JS"
console.log(prefix);

/*User.find({}).remove(function() {
  User.create({
    provider: "local",
    name: "Test User",
    email: "test@test.com",
    password: "test"
  }, {
    provider: "local",
    role: "admin",
    name: "Admin",
    email: "admin@admin.com",
    password: "admin"
  }, function(err, user) {
      if(err){
        console.log(prefix+" "+err);
      }else if(!user){
        console.log(prefix+" users populated but user null " + JSON.stringify(user));
      }
      console.log(prefix+" users populated " + JSON.stringify(user));
    }
  );
});*/

console.log("Empieza con subscriptionTypes");
SubscriptionType.findOne({"name": "basic"}).exec(function(err, basic) {
  if(!basic){
    SubscriptionType.create({
      name: "basic",
      numberOfActives: 10,
      hasPubli: true
    }, function(err, subscriptionType) {
        if(err){
          console.log(prefix+" "+err);
        }else if(!subscriptionType){
          console.log(prefix+" subscriptionTypes populated but user null " + JSON.stringify(subscriptionType));
        }
        console.log(prefix+" subscriptionTypes populated " + JSON.stringify(subscriptionType));
      }
    );
  }
});

console.log("Empieza con usuarios (admin)");
User.findOne({"role": "admin"}).exec(function(err, admin) {
  console.log(admin)
  if(!admin){
    User.create({
      provider: "local",
      role: "admin",
      name: "Admin",
      email: "admin@admin.com",
      password: "admin"
    }, function(err, user) {
        if(err){
          console.log(prefix+" "+err);
        }else if(!user){
          console.log(prefix+" users populated but user null " + JSON.stringify(user));
        }
        console.log(prefix+" users populated " + JSON.stringify(user));
      }
    );
  }
});

console.log("Empieza con oficios");
var professions = ["Camarero", "Vendedor", "Profesor", "Comercial", "Médico", "Directivo", "Asesor Financiero", "Politico", "Futbolista", "Periodista", "Peluquero", "Consultor"];

/*professions.forEach (function (professionName) {
  console.log("prefession name: " + professionName);
  Profession
    .findOne({"name": professionName})
    .exec(function(err, profession){
      if (err) {
        console.log("find err: " + err);
      }
      if (!profession) {
        console.log("found profession: " +  profession);
        Profession.create({"name": professionName},  function(err, profession) {
          if(err){
            console.log("create err: " + err);
          }
          console.log("create profession.name: " +  profession);
        });
      } else {
        console.log("found profession.name: " + profession.name);
      }
    });
});*/

console.log("Empieza con areas/categorias/topics/questions");
var areas = [{hardId: "0", name:"ESTUDIOS INTEGRALES"},
  {hardId: "2", name: "AREA PERSONAL", categories: [
    {name:"Humildad", topics: [
      {name: "Autoconocimiento", questions: [ { name: "Conoce bien sus propias fortalezas, habilidades y áreas de mejora" }]}
      , {name: "Capacidad de aprendizaje", questions: [ { name: "Tiene facilidad para integrar y aplicar nuevas herramientas, conocimientos y formas de hacer" }]}
      , {name: "Curiosidad", questions: [ { name: "Demuestra interés y proactividad por conocer y aplicar nuevas herramientas, conocimientos y formas de hacer" }]}
      , {name: "Modestia", questions: [ { name: "Se manifiesta sin alardes de sus fortalezas y conocimientos, y tiende a minimizar sus propios méritos" }]}
      , {name: "Discreccion", questions: [ { name: "Se proyecta hacia el exterior de manera tranquila, sin protagonismo y sin mostrar necesidad de destacar sobre el resto" }]}
      , {name: "Ejemplaridad", questions: [ { name: "Sirve de modelo para el resto de personas a través de su actuación y sus valores" }]}
      , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
      , {name: "Cercania", questions: [ { name: "Se aproxima a las personas de su entorno de forma natural, proactiva y empática" }]}
      , {name: "Respeto", questions: [ { name: "Acepta e integra la diversidad en las personas de su entorno, de manera abierta y sin prejuicios" }]}
      , {name: "Equilibrio", questions: [ { name: "Mantiene una línea homogénea de pensamiento y actuación, sin alteraciones y con serenidad" }]}
    ]}
    , {name:"Responsabilidad", topics: [
      {name: "Compromiso", questions: [ { name: "Se implica en los proyectos hasta el final, sin dejarlos a medias, y finaliza los trabajos a los que se compromete" }]}
      , {name: "Madurez", questions: [ { name: "Actúa de manera reflexiva y sin altibajos, y analiza las situaciones desde un punto de vista serio y coherente" }]}
      , {name: "Sensatez", questions: [ { name: "Aplica sentido común a sus actos y reflexiona en lugar de actuar de forma impulsiva" }]}
      , {name: "Libertad", questions: [ { name: "Se muestra con la seguridad y confianza suficientes para opinar y actuar según su propio criterio" }]}
      , {name: "Consciencia", questions: [ { name: "Tiene un buen conocimiento de sí mismo y del contexto en el que se mueve" }]}
      , {name: "Lucidez", questions: [ { name: "Tiene claridad de ideas para analizar las diferentes situaciones y adoptar la mejor solución en cada caso" }]}
      , {name: "Justicia", questions: [ { name: "Aplica el mismo criterio y baremo a todas las personas que le rodean, sin distinciones ni favoritismos" }]}
      , {name: "Coherencia", questions: [ { name: "Hace lo que dice: actúa de forma adecuada al mensaje que transmite" }]}
      , {name: "Honestidad", questions: [ { name: "Se muestra tal y como es, va de frente y sin doble cara" }]}
      , {name: "Valentia", questions: [ { name: "Actúa con seguridad en sí mismo y adoptando riesgos calculados" }]}
    ]}
    , {name:"Empatia", topics: [
      {name: "Afectividad", questions: [ { name: "Actúa con respeto y cariño hacia las personas que le rodean" }]}
      , {name: "Comprension", questions: [ { name: "Integra las opiniones y situaciones de los demás y se pone en su lugar" }]}
      , {name: "Cercania", questions: [ { name: "Se aproxima a las personas de su entorno de forma natural, proactiva y empática" }]}
      , {name: "Generosidad", questions: [ { name: "Es capaz de esforzarse para ayudar a los demás de manera desinteresada" }]}
      , {name: "Inteligencia emocional", questions: [ { name: "Es capaz de adaptar su actuación en función de las emociones de las personas que le rodean para no herir sus sentimientos" }]}
      , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
      , {name: "Intuicion", questions: [ { name: "Sabe captar los detalles de cada situación, analizarlos y reflexionar sobre sus posibles consecuencias" }]}
      , {name: "Sensibilidad", questions: [ { name: "Muestra humanidad y actúa de forma cuidadosa ante las emociones de las personas que le rodean" }]}
      , {name: "Humildad", questions: [ { name: "Se muestra modesto y accesible, sin alardes de sus propios logros, y se coloca a la misma altura que el resto de personas" }]}
      , {name: "Observacion", questions: [ { name: "Está atento a detalles y gestos, y los tiene en cuenta para generar su propia opinión y decidir su forma de actuar" }]}
      ]}
    , {name:"Respeto", topics: [
      {name: "Tolerancia", questions: [ { name: "Acepta la diversidad de forma natural y actúa de manera reflexiva ante formas de pensar diferentes a la suya" }]}
      , {name: "Civismo", questions: [ { name: "Actúa de manera natural en el marco de las reglas comunes de convivencia" }]}
      , {name: "Diversidad", questions: [ { name: "Es capaz de adaptar su actuación en función de las circunstancias del entorno y del contexto en el que se mueve" }]}
      , {name: "Sensibilidad", questions: [ { name: "Muestra humanidad y actúa de forma cuidadosa ante las emociones de las personas que le rodean" }]}
      , {name: "Humildad", questions: [ { name: "Se muestra modesto y accesible, sin alardes de sus propios logros, y se coloca a la misma altura que el resto de personas" }]}
      , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
      , {name: "Comprension", questions: [ { name: "Integra las opiniones y situaciones de los demás y se pone en su lugar" }]}
      , {name: "Empatia", questions: [ { name: "Se pone en lugar del otro para tratar de entender su posición y de integrarla al construir su propia opinión" }]}
      , {name: "Autocontrol", questions: [ { name: "Mantiene la calma en situaciones de tensión, y actúa de manera reflexiva sin dejarse llevar por las emociones" }]}
      , {name: "Inteligencia emocional", questions: [ { name: "Es capaz de adaptar su actuación en función de las emociones de las personas que le rodean para no herir sus sentimientos" }]}
      ]}
    , {name:"Energia", topics: [
      {name: "Alegria", questions: [ { name: "Muestra siempre una imagen alegre y una actitud abierta y sonriente" }]}
      , {name: "Positivismo", questions: [ { name: "Tiende a ver siempre la parte positiva de las cosas y a entenderlas en clave de oportunidad" }]}
      , {name: "Resiliencia", questions: [ { name: "Es capaz de recuperarse de las situaciones adversas y de volver a su actitud original" }]}
      , {name: "Constancia", questions: [ { name: "Mantiene el nivel de esfuerzo y compromiso durante tiempo prolongado y no decae en el empeño" }]}
      , {name: "Esfuerzo", questions: [ { name: "Pone intensidad en sus acciones y se empeña en alcanzar el resultado" }]}
      , {name: "Vitalidad", questions: [ { name: "Se muestra siempre activo y enérgico, y transmite esta energía en su entorno" }]}
      , {name: "Coraje", questions: [ { name: "Se enfrenta con firmeza y valentía ante situaciones complicadas o injustas" }]}
      , {name: "Superacion", questions: [ { name: "Se sobrepone a la adversidad, es inconformista  y trata siempre de superar los objetivos" }]}
      , {name: "Iniciativa", questions: [ { name: "Actúa y pone en marcha proyectos por decisión propia sin esperar a que se lo ordenen" }]}
      , {name: "Pasion", questions: [ { name: "Se implica emocionalmente y pone toda su fuerza personal en los proyectos" }]}
      ]}
    , {name:"Espiritu de superacion", topics: [
      {name: "Ilusion", questions: [ { name: "Se motiva por alcanzar los objetivos previstos y los integra como oportunidades de crecimiento personal" }]}
      , {name: "Vision", questions: [ { name: "Se proyecta en el futuro para visualizar objetivos y retos a poner en marcha" }]}
      , {name: "Constancia", questions: [ { name: "Mantiene el nivel de esfuerzo y compromiso durante tiempo prolongado y no decae en el empeño" }]}
      , {name: "Fuerza", questions: [ { name: "Imprime vigor y energía a sus acciones, y transmite esta sensación en su entorno" }]}
      , {name: "Positivismo", questions: [ { name: "Tiende a ver siempre la parte positiva de las cosas y a entenderlas en clave de oportunidad" }]}
      , {name: "Autoconfianza", questions: [ { name: "Se muestra seguro de sus opiniones y acciones, y traslada esta seguridad a sus decisiones" }]}
      , {name: "Paciencia", questions: [ { name: "Se muestra tranquilo y sin perder los nervios ante los errores o las dificultades" }]}
      , {name: "Espiritu de mejora", questions: [ { name: "Es inconformista con los objetivos y los resultados, y trata siempre de superarse y de alcanzar nuevos retos" }]}
      , {name: "Auto motivacion", questions: [ { name: "Busca retos y objetivos que le animen para alcanzar el resultado" }]}
      , {name: "Orientado a la accion", questions: [ { name: "Demuestra un alto nivel de actividad y es ágil y rápido a la hora de poner acciones en marcha" }]}
      ]}
    , {name:"Integridad", topics: [
      {name: "Dignidad", questions: [ { name: "Se mantiene firme en sus valores sin rendirse ante presiones del entorno" }]}
      , {name: "Etica", questions: [ { name: "Aplica valores de justicia y de respeto a los derechos de los demás " }]}
      , {name: "Confianza", questions: [ { name: "Se muestra seguro de sus opiniones y acciones, y traslada esta seguridad a sus decisiones" }]}
      , {name: "Honestidad", questions: [ { name: "Se muestra tal y como es, va de frente y sin doble cara" }]}
      , {name: "Nobleza", questions: [ { name: "Actúa con franqueza y honestidad, de forma confiable para las personas de su entorno" }]}
      , {name: "Sinceridad", questions: [ { name: "Dice siempre la verdad de forma honesta y respetuosa" }]}
      , {name: "Honradez", questions: [ { name: "Actúa de forma recta y justa, en función de lo que considera correcto y adecuado" }]}
      , {name: "Equilibrio", questions: [ { name: "Mantiene una línea homogénea de pensamiento y actuación, sin alteraciones y con serenidad" }]}
      , {name: "Justicia", questions: [ { name: "Aplica el mismo criterio y baremo a todas las personas que le rodean, sin distinciones ni favoritismos" }]}
      , {name: "Responsabilidad", questions: [ { name: "Asume de manera natural sus aciertos y sus errores, sin buscar otros responsables" }]}
      ]}
    , {name:"Equilibrio", topics: [
      {name: "Autoconfianza", questions: [ { name: "Se muestra seguro de sus opiniones y acciones, y traslada esta seguridad a sus decisiones" }]}
      , {name: "Serenidad", questions: [ { name: "Se muestra calmado y reflexivo ante situaciones de tensión" }]}
      , {name: "Justicia", questions: [ { name: "Aplica el mismo criterio y baremo a todas las personas que le rodean, sin distinciones ni favoritismos" }]}
      , {name: "Sensatez", questions: [ { name: "Aplica sentido común a sus actos y reflexiona en lugar de actuar de forma impulsiva" }]}
      , {name: "Personalidad", questions: [ { name: "Tiene claras sus convicciones y valores, y actúa conforme a ellos" }]}
      , {name: "Espiritu de superacion", questions: [ { name: "Se sobrepone a la adversidad, es inconformista  y trata siempre de superar los objetivos" }]}
      , {name: "Autoconocimiento", questions: [ { name: "Conoce bien sus propias fortalezas, habilidades y áreas de mejora" }]}
      , {name: "Autoestima", questions: [ { name: "Muestra seguridad y confianza en sus propias fortalezas y valores" }]}
      , {name: "Autocontrol", questions: [ { name: "Mantiene la calma en situaciones de tensión, y actúa de manera reflexiva sin dejarse llevar por las emociones" }]}
      , {name: "Altruismo", questions: [ { name: "Orienta sus acciones en beneficio de los demás y de forma desinteresada" }]}
      ]}
  ]},
  {hardId: "1", name:"AREA COMPETENCIAL", categories: [
    {name:"Vision y estrategia", topics: [
        {name: "Conocimiento del entorno", questions: [ { name: "Busca información del entorno y está actualizado sobre lo que ocurre a su alrededor" }]}
        , {name: "Observacion", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
        , {name: "Curiosidad", questions: [ { name: "Demuestra interés y proactividad por conocer y aplicar nuevas herramientas, conocimientos y formas de hacer" }]}
        , {name: "Imaginacion", questions: [ { name: "Tiene capacidad para idear y visualizar situaciones o proyectos innovadores" }]}
        , {name: "Creatividad", questions: [ { name: "Propone soluciones diferentes e innovadoras, saliéndose de los estándares y de las propuestas clásicas" }]}
        , {name: "Lucidez", questions: [ { name: "Tiene claridad de ideas para analizar las diferentes situaciones y adoptar la mejor solución en cada caso" }]}
        , {name: "Pensamiento estrategico", questions: [ { name: "Proyecta sus planes a medio y largo plazo, sin centrarse sólo en las necesidades inmediatas" }]}
        , {name: "Influencia", questions: [ { name: "Utiliza sus recursos para convencer a los demás y para influir sobre su modo de actuar" }]}
        , {name: "Capacidad de adaptacion", questions: [ { name: "Se muestra flexible y adapta su comportamiento en función de las diferentes características del entorno" }]}
        , {name: "Espiritu de superacion", questions: [ { name: "Se sobrepone a la adversidad, es inconformista  y trata siempre de superar los objetivos" }]}
      ]}
    ,{name:"Liderazgo", topics: [
        {name: "Vision", questions: [ { name: "Se proyecta en el futuro para visualizar objetivos y retos a poner en marcha" }]}
        , {name: "Inspirador", questions: [ { name: "Con su forma de pensar y de actuar sirve de impulso y de ánimo al resto de personas" }]}
        , {name: "Motivacion", questions: [ { name: "Se mantiene firme en sus convicciones y las defiende ante poscicionamientos diferente" }]}
        , {name: "Compromiso", questions: [ { name: "Se implica en los proyectos hasta el final, sin dejarlos a medias, y asume como propios los objetivos a alcanzar" }]}
        , {name: "Espiritu de servicio", questions: [ { name: "Se muestra disponible para ayudar a los demás siempre que lo necesitan" }]}
        , {name: "Ejemplaridad", questions: [ { name: "Sirve de modelo para el resto de personas a través de su actuación y sus valores" }]}
        , {name: "Influencia", questions: [ { name: "Utiliza sus recursos para convencer a los demás y para influir sobre su modo de actuar" }]}
        , {name: "Organizacion", questions: [ { name: "Actúa de forma ordenada y planificada, y mantiene todos los detalles bajo control" }]}
        , {name: "Desarrollo de personas", questions: [ { name: "Ayuda con sus recursos personales al crecimiento personal o profesional de los que le rodean" }]}
        , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
      ]}
    ,{name:"Efectividad personal", topics: [
        {name: "Priorizacion", questions: [ { name: "Identifica las necesidades más urgentes y dedica su esfuerzo a resolverlas en primer lugar" }]}
        , {name: "Organizacion", questions: [ { name: "Actúa de forma ordenada y planificada, y mantiene todos los detalles bajo control" }]}
        , {name: "Orientacion a resultados", questions: [ { name: "Plantea y pone en marcha acciones que conlleven mejoras y resultados medibles" }]}
        , {name: "Trabajo en equipo", questions: [ { name: "Se integra en el grupo, escucha las opiniones del resto, propone ideas de mejora y acepta las decisiones del equipo como propias" }]}
        , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
        , {name: "Comunicacion efectiva", questions: [ { name: "Traslada el mensaje de forma clara y sin rodeos, adaptando la comunicación al receptor y asegurando la llegada del mensaje" }]}
        , {name: "Adaptacion", questions: [ { name: "Se muestra flexible y adapta su comportamiento en función de las diferentes características del entorno" }]}
        , {name: "Sentido del humor", questions: [ { name: "Se muestra predispuesto a valorar las situaciones desde un punto de vista alegre y desenfadado, evitando el dramatismo" }]}
        , {name: "Autocontrol", questions: [ { name: "Mantiene la calma en situaciones de tensión, y actúa de manera reflexiva sin dejarse llevar por las emociones" }]}
        , {name: "Orientado a la accion", questions: [ { name: "Demuestra un alto nivel de actividad y es ágil y rápido a la hora de poner acciones en marcha" }]}
      ]}
    ,{name:"Trabajo en equipo", topics: [
        {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
        , {name: "Respeto", questions: [ { name: "Acepta e integra la diversidad en las personas de su entorno, de manera abierta y sin prejuicios" }]}
        , {name: "Diversidad", questions: [ { name: "Es capaz de adaptar su actuación en función de las circunstancias del entorno y del contexto en el que se mueve" }]}
        , {name: "Compromiso", questions: [ { name: "Se implica en los proyectos hasta el final, sin dejarlos a medias, y asume como propios los objetivos a alcanzar" }]}
        , {name: "Comunicacion efectiva", questions: [ { name: "Traslada el mensaje de forma clara y sin rodeos, adaptando la comunicación al receptor y asegurando la llegada del mensaje" }]}
        , {name: "Participacion", questions: [ { name: "Se muestra activo en el proceso de trabajo, aportando opiniones, ideas y esfuerzo para alcanzar el objetivo común" }]}
        , {name: "Exigencia", questions: [ { name: "Busca alcanzar objetivos retadores, se muestra inconformista y trabaja para lograr un nivel alto de calidad en el resultado" }]}
        , {name: "Sentido del humor", questions: [ { name: "Muestra habitualmente actitud positiva y espíritu alegre para enfrentar diferentes situaciones" }]}
        , {name: "Resolucion de problemas", questions: [ { name: "Se muestra ágil y efectivo en el análisis de las situaciones y en la toma de la mejor decisión para solucionar el caso" }]}
        , {name: "Autonomia", questions: [ { name: "Dispone de recursos y capacidades para tomar decisiones y actuar de forma individual, sin depender de otras personas" }]}
      ]}
    ,{name:"Iniciativa", topics: [
        {name: "Espiritu mejora", questions: [ { name: "Es inconformista con los objetivos y los resultados, y trata siempre de superarse y de alcanzar nuevos retos" }]}
        , {name: "Autonomia", questions: [ { name: "Dispone de recursos y capacidades para tomar decisiones y actuar de forma individual, sin depender de otras personas" }]}
        , {name: "Autoconfianza", questions: [ { name: "Se muestra confiado en sus propuestas y es capaz de tomar decisiones sin titubeos y con alto grado de autoconfianza" }]}
        , {name: "Asuncion de riesgos", questions: [ { name: "Identifica los beneficios y riesgos, y es capaz de arriesgar de forma calculada para obtener los resultados esperados" }]}
        , {name: "Responsabilidad", questions: [ { name: "Asume de manera natural sus aciertos y sus errores, sin buscar otros responsables" }]}
        , {name: "Orientacion a resultados", questions: [ { name: "Busca siempre en sus acciones la efectividad y la obtención de resultados medibles " }]}
        , {name: "Esfuerzo", questions: [ { name: "Pone intensidad en sus acciones y se empeña en alcanzar el resultado" }]}
        , {name: "Resolucion de problemas", questions: [ { name: "Se muestra ágil y efectivo en el análisis de las situaciones y en la toma de la mejor decisión para solucionar el caso" }]}
        , {name: "Proactividad", questions: [ { name: "Toma la iniciativa para proponer ideas y actuar, sin necesidad de que otros se lo pidan o lo hagan antes que él" }]}
        , {name: "Anticipacion", questions: [ { name: "Busca las soluciones sin esperar a que se produzca el problema, intentando situarse por delante de la necesidad" }]}
      ]}
    ,{name:"Capacidad Analitica", topics: [
        {name: "Conocimiento del entorno", questions: [ { name: "Busca información del entorno y está actualizado sobre lo que ocurre a su alrededor" }]}
        , {name: "Definicion del problema"}
        , {name: "Estructuracion de problemas", questions: [ { name: "Es capaz de analizar problemas complejos dividiéndolos en partes más sencillas de forma separada" }]}
        , {name: "Razonamiento Logico"}
        , {name: "Pensamiento lateral", questions: [ { name: "Es capaz de resolver problemas con ideas creativas e innovadoras, saliéndose de los patrones de pensamiento clásicos o habituales" }]}
        , {name: "Priorizacion", questions: [ { name: "Identifica las necesidades más urgentes y dedica su esfuerzo a resolverlas en primer lugar" }]}
        , {name: "Metodologia", questions: [ { name: "Aplica un estructura y un proceso ordenado de trabajo para llevar a cabo un proyecto o llegar a una decisión" }]}
        , {name: "Busqueda y tratamiento informacion", questions: [ { name: "Conoce los medios para obtener la información que necesita, y utiliza ésta de forma adecuada para tomar una decisión" }]}
        , {name: "Capacidad de sintesis", questions: [ { name: "Es capaz de manejar grandes volúmenes de información y extraer lo fundamental para trabajar con ello" }]}
        , {name: "Tolerancia stress", questions: [ { name: "Se muestra efectivo en su proceso de trabajo y en sus decisiones ante situaciones de mucha presión del entorno" }]}
      ]}
    ,{name:"Comunicacion", topics: [
        {name: "Empatia", questions: [ { name: "Se pone en lugar del otro para tratar de entender su posición y de integrarla al construir su propia opinión" }]}
        , {name: "Comunicacion escrita", questions: [ { name: "Es capaz de trasladar por escrito sus ideas con un lenguaje sencillo, correcto, entendible y estructurado" }]}
        , {name: "Comunicacion verbal", questions: [ { name: "Utiliza un lenguaje claro y entendible, y aplica técnicas (silencios, inflexiones, emocionalidad…) para dar más fuerza a su comunicación" }]}
        , {name: "Comunicacion no verbal", questions: [ { name: "Controla y tiliza sus recursos de comunicación corporal para dar coherencia y fuerza al lenguaje que utiliza" }]}
        , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del resto, y las integra para construir sus propias convicciones" }]}
        , {name: "Claridad mensaje", questions: [ { name: "Traslada su mensaje de forma clara, estructurada, sin rodeos y con un lenguaje entendible" }]}
        , {name: "Adaptacion mensaje", questions: [ { name: "Es capaz de utilizar diferentes recursos y niveles de comunicación en función del nivel o de la disposición del receptor" }]}
        , {name: "Pasion", questions: [ { name: "Se implica emocionalmente y pone toda su fuerza personal en los proyectos" }]}
        , {name: "Medios tecnicos", questions: [ { name: "Conoce y utiliza soportes y herramientas técnicas que le puedan servir de ayuda en su comunicación" }]}
        , {name: "Tolerancia stress", questions: [ { name: "Se muestra efectivo en su proceso de trabajo y en sus decisiones ante situaciones de mucha presión del entorno" }]}
      ]}
    ,{name:"Innovacion", topics: [
        {name: "Curiosidad", questions: [ { name: "Demuestra interés y proactividad por conocer y aplicar nuevas herramientas, conocimientos y formas de hacer" }]}
        , {name: "Observacion", questions: [ { name: "Está atento a detalles y gestos en su entorno, y los tiene en cuenta para generar su propia opinión y decidir su forma de actuar" }]}
        , {name: "Espiritu mejora", questions: [ { name: "Es inconformista con los objetivos y los resultados, y trata siempre de superarse y de alcanzar nuevos retos" }]}
        , {name: "Asuncion riesgos", questions: [ { name: "Identifica los beneficios y riesgos, y es capaz de arriesgar de forma calculada para obtener los resultados esperados" }]}
        , {name: "Capacidad de aprendizaje"}
        , {name: "Pensamiento lateral", questions: [ { name: "Es capaz de resolver problemas con ideas creativas e innovadoras, saliéndose de los patrones de pensamiento clásicos o habituales" }]}
        , {name: "Tolerancia stress", questions: [ { name: "Se muestra efectivo en su proceso de trabajo y en sus decisiones ante situaciones de mucha presión del entorno" }]}
        , {name: "Creatividad", questions: [ { name: "Propone soluciones diferentes e innovadoras, saliéndose de los estándares y de las propuestas clásicas" }]}
        , {name: "Espiritu critico", questions: [ { name: "Se muestra inconformista con el resultado, y busca siempre las posibilidades y la forma de mejorarlo" }]}
        , {name: "Pasion", questions: [ { name: "Se implica emocionalmente y pone toda su fuerza personal en los proyectos" }]}
      ]}
    ]},
  {hardId: "3", name:"AREA PROFESIONAL", categories: [
    {name:"Camarero", profession: { name: "Camarero" }, topics: [
        {name: "Asesoramiento", questions: [ { name: "Conoce los productos, sus características y usos y los recomienda en función de las necesidades o gustos del cliente" }]}
        , {name: "Amabilidad", questions: [ { name: "Se muestra educado con los clientes y permanece atento y disponible para atender sus necesidades" }]}
        , {name: "Empatia", questions: [ { name: "Se coloca en la posición del cliente para entender mejor sus necesidades y gustos y así dar un mejor servicio" }]}
        , {name: "Conocimiento", questions: [ { name: "Conoce toda la gama de productos y servicios disponibles para sus clientes, y está al día de novedades y cambios" }]}
        , {name: "Cercania", questions: [ { name: "Está atento a los gestos de los clientes y se muestra amable y sencillo con ellos" }]}
        , {name: "Proactividad", questions: [ { name: "Toma la iniciativa para dirigirse al cliente y atenderle sin necesidad de que éste reclame su atención" }]}
        , {name: "Escucha", questions: [ { name: "Indaga en las necesidades y gustos del cliente a través de preguntas para prestar el servicio adecuado en cada caso" }]}
        , {name: "Resolutividad", questions: [ { name: "Demuestra agilidad y eficacia a la hora de atender y resolver las necesidades planteadas por los clientes" }]}
        , {name: "Comunicacion", questions: [ { name: "Utiliza un lenguaje verbal y no verbal educado, conciso y claro a la hora de atender a los clientes" }]}
        , {name: "Despedida", questions: [ { name: "Agradece la visita y se despide de los clientes de forma amable y educada" }]}
      ]}
    ,{name:"Vendedor", profession: { name: "Vendedor" }, topics: [
        {name: "Acogida", questions: [ { name: "Toma la iniciativa para dirigirse al cliente y atenderle sin necesidad de que éste reclame su atención" }]}
        , {name: "Asesoramiento", questions: [ { name: "Conoce los productos, sus características y usos y los recomienda en función de las necesidades o gustos del cliente" }]}
        , {name: "Amabilidad", questions: [ { name: "Se muestra educado con los clientes y permanece atento y disponible para atender sus necesidades" }]}
        , {name: "Empatia", questions: [ { name: "Se coloca en la posición del cliente para entender mejor sus necesidades y gustos y así dar un mejor servicio" }]}
        , {name: "Conocimiento", questions: [ { name: "Conoce toda la gama de productos y servicios disponibles para sus clientes, y está al día de novedades y cambios" }]}
        , {name: "Cercania", questions: [ { name: "Está atento a los gestos de los clientes y se muestra amable y sencillo con ellos" }]}
        , {name: "Escucha", questions: [ { name: "Indaga en las necesidades y gustos del cliente a través de preguntas para prestar el servicio adecuado en cada caso" }]}
        , {name: "Resolutividad", questions: [ { name: "Demuestra agilidad y eficacia a la hora de atender y resolver las necesidades planteadas por los clientes" }]}
        , {name: "Comunicacion", questions: [ { name: "Utiliza un lenguaje verbal y no verbal educado, conciso y claro a la hora de atender a los clientes" }]}
        , {name: "Despedida", questions: [ { name: "Agradece la visita y se despide de los clientes de forma amable y educada" }]}
      ]}
    ,{name:"Profesor", profession: { name: "Profesor" }, topics: [
        {name: "Conocimiento", questions: [ { name: "Domina el tema sobre el que imparte y es capaz de responder a las preguntas que se le planteen sobre el mismo" }]}
        , {name: "Adaptabilidad", questions: [ { name: "Es capaz de modificar el contenido, el nvel, el discurso o el ritmo en función de las características del grupo de destinatarios" }]}
        , {name: "Comunicacion", questions: [ { name: "Utiliza un lenguaje claro y entendible, y aplica técnicas (silencios, inflexiones, emocionalidad…) para dar más fuerza a su comunicación" }]}
        , {name: "Nuevas tecnologias", questions: [ { name: "Conoce los recursos tecnológicos más modernos y los utiliza en la impartición para hacerla más efectiva y amena" }]}
        , {name: "Justicia", questions: [ { name: "Aplica el mismo criterio y baremo a todas las personas de su grupo, sin distinciones ni favoritismos" }]}
        , {name: "Planificacion", questions: [ { name: "Dota de estructura, orden y prioridad a los contenidos, y organiza la impartición con criterios de eficacia y calidad en la docencia" }]}
        , {name: "Motivacion", questions: [ { name: "Busca retos y objetivos que le animen para alcanzar el resultado" }]}
        , {name: "Curiosidad", questions: [ { name: "Demuestra interés y proactividad por conocer y aplicar nuevas herramientas, conocimientos y formas de hacer" }]}
        , {name: "Escucha", questions: [ { name: "Se interesa e indaga de forma activa en las opiniones del grupo, y las integra para mejorar la calidad de la impartición" }]}
        , {name: "Espiritu mejora", questions: [ { name: "Es inconformista con los objetivos y los resultados, y trata siempre de superarse y de alcanzar nuevos retos" }]}
        , {name: "Didactismo", questions: [ { name: "Tiene habilidad y recursos para impartir los contenidos de forma amena, agradable y entendible" }]}
      ]}
    ,{name:"Comercial", profession: { name: "Comercial" }, topics: [
        {name: "Asesoramiento", questions: [ { name: "Conoce los productos, sus características y usos y los recomienda en función de las necesidades o gustos del cliente" }]}
        , {name: "Amabilidad", questions: [ { name: "Se muestra educado con los clientes y permanece atento y disponible para atender sus necesidades" }]}
        , {name: "Empatia", questions: [ { name: "Se coloca en la posición del cliente para entender mejor sus necesidades y gustos y así dar un mejor servicio" }]}
        , {name: "Conocimiento", questions: [ { name: "Conoce toda la gama de productos y servicios disponibles para sus clientes, y está al día de novedades y cambios" }]}
        , {name: "Organizacion", questions: [ { name: "Actúa de forma ordenada y planificada, y mantiene todos los detalles bajo control" }]}
        , {name: "Proactividad", questions: [ { name: "Toma la iniciativa para dirigirse al cliente y atenderle sin necesidad de que éste reclame su atención" }]}
        , {name: "Escucha", questions: [ { name: "Indaga en las necesidades y gustos del cliente a través de preguntas para prestar el servicio adecuado en cada caso" }]}
        , {name: "Resolutividad", questions: [ { name: "Demuestra agilidad y eficacia a la hora de atender y resolver las necesidades planteadas por los clientes" }]}
        , {name: "Comunicacion", questions: [ { name: "Utiliza un lenguaje verbal y no verbal educado, conciso y claro a la hora de atender a los clientes" }]}
        , {name: "Negociacion", questions: [ { name: "Es capaz de alcanzar con sus clientes acuerdo comerciales favorables para las dos partes" }]}
      ]}
    ,{name:"Médico", profession: { name: "Médico" }}
    ,{name:"Directivo", profession: { name: "Directivo" }}
    ,{name:"Asesor Financiero", profession: { name: "Asesor Financiero" }, topics: [
        {name: "Confianza"}
        , {name: "Asesoramiento"}
        , {name: "Iniciativa"}
        , {name: "Vision y conocimiento del entorno"}
        , {name: "Conocimiento financiero"}
        , {name: "Orientacion a resultados"}
        , {name: "Escucha"}
        , {name: "Etica"}
        , {name: "Comunicacion"}
        , {name: "Disponibilidad"}
      ]}
    ,{name:"Politico", profession: { name: "Politico" }}
    ,{name:"Futbolista", profession: { name: "Futbolista" }}
    ,{name:"Periodista", profession: { name: "Periodista" }}
    ,{name:"Peluquero", profession: { name: "Peluquero" }, topics: [
        {name: "Escucha", questions: [ { name: "Indaga en las necesidades y gustos del cliente a través de preguntas para prestar el servicio adecuado en cada caso" }]}
        , {name: "Asesoramiento", questions: [ { name: "Conoce los productos, sus características y usos y los recomienda en función de las necesidades o gustos del cliente" }]}
        , {name: "Oficio", questions: [ { name: "Utiliza con habilidad las técnicas y herramentas de su profesión para dar un buen servicio al cliente" }]}
        , {name: "Rapidez", questions: [ { name: "Trabaja con agilidad y eficacia, sin hacer perder el tiempo al cliente" }]}
        , {name: "Respeto", questions: [ { name: "Se dirige al cliente con educación y de una manera amable y profesional" }]}
        , {name: "Atencion", questions: [ { name: "Está pendiente de los gestos del cliente y de los detalles para adaptar su trabajo en función de los mismos" }]}
        , {name: "Amabilidad", questions: [ { name: "Se muestra educado con los clientes y permanece atento y disponible para atender sus necesidades" }]}
        , {name: "Iniciativa", questions: [ { name: "Propone alternativas y soluciones para satisfacer al cliente, sin necesidad de que éste se lo pida" }]}
        , {name: "Creatividad", questions: [ { name: "Propone soluciones diferentes e innovadoras, saliéndose de los estándares y de las propuestas clásicas" }]}
        , {name: "Despedida", questions: [ { name: "Agradece la visita y se despide de los clientes de forma amable y educada" }]}
      ]}
    ,{name:"Consultor", profession: { name: "Consultor" }, topics: [
        {name: "Asesoramiento", questions: [ { name: "Conoce y traslada al cliente diferentes alternativas de solución para satisfacer sus necesidades" }]}
        , {name: "Amabilidad", questions: [ { name: "Se muestra educado con los clientes y permanece atento y disponible para atender sus necesidades" }]}
        , {name: "Empatia", questions: [ { name: "Se coloca en la posición del cliente para entender mejor sus necesidades y así dar un mejor servicio" }]}
        , {name: "Conocimiento", questions: [ { name: "Domina el ámbito de actuación sobre el que el cliente requiere su trabajo" }]}
        , {name: "Cercania", questions: [ { name: "Se muestra disponible y atento para atender al cliente y satisfacer sus necesidades" }]}
        , {name: "Proactividad", questions: [ { name: "Toma la iniciativa para dirigirse al cliente, atenderle y proponerle productos o servicios sin necesidad de que éste reclame su atención" }]}
        , {name: "Escucha", questions: [ { name: "Indaga en las necesidades y gustos del cliente a través de preguntas para prestar el servicio adecuado en cada caso" }]}
        , {name: "Resolutividad", questions: [ { name: "Demuestra agilidad y eficacia a la hora de atender y resolver las necesidades planteadas por los clientes" }]}
        , {name: "Comunicacion", questions: [ { name: "Utiliza un lenguaje verbal y no verbal educado, conciso y claro a la hora de atender a los clientes" }]}
        , {name: "Confianza", questions: [ { name: "Se muestra seguro de sus opiniones y acciones, y traslada esta seguridad a sus decisiones" }]}
      ]}
    ]}
  ];

recursiveAddAreas(areas, 0, function(){
  console.log("DONE POPULATING");
  areas.forEach (function (area) {
    if(area.categories) console.log("area " + area.name + " categories.length: " + area.categories.length);
  });
});

function recursiveAddAreas(areas, position, done){
  console.log("recursiveAddArea(" + position + ") adding " + areas[position].name);
  Area
  .findOne({"name": areas[position].name})
  .exec(function(err, area){
      if (err) {
        console.log("find err: " + err);
      }
      if (!area) {
        console.log("not found area: " +  area + " " + areas[position].name);
        Area.create({name: areas[position].name, hardId: areas[position].hardId},  function(err, area) {
          if(err){
            console.log("create err: " + err);
          }
          console.log("create area.name: " +  area.name);
          if(areas[position].categories) {
            recursiveAddCategories(area, areas[position].categories, 0, function(category){
              position = position+1;
              if(position<areas.length){
                recursiveAddAreas(areas, position, function(){
                  done();
                });
              }else{
                done();
              }
            });
          }else{
            position = position+1;
            if(position<areas.length){
              recursiveAddAreas(areas, position, function(){
                done();
              });
            }else{
              done();
            }
          }
        });
      } else {
        console.log("found area.name: " + area.name);
        area.hardId = areas[position].hardId;
        area.save();
      }
  });
}

function recursiveAddCategories(area, categories, position, done){
  //console.log("recursiveAddCategories(" + position + ") adding " + categories[position].name + " into " + area.name);
  var categoryPre = categories[position];
  Category
  .create({name: categoryPre.name}, function(err, category){
    if(err){

    }else{
      //Add to area
      area.categories.push(category);
      if(categories[position].profession){
        addProfession(area, categories[position].profession, category, function(profession){
          area.categories[position].profession = profession;
          area.save(function (err, area){
            addTopicsOrContinue(area, categories, category, position, done);
          });
        });
      }else{
        area.save(function (err, area){
          addTopicsOrContinue(area, categories, category, position, done);
        });
      }
    }
  });
}

function addProfession(area, professionPre, category, done){
  var professionName = professionPre.name;
  Profession
    .findOne({"name": professionName})
    .exec(function(err, profession){
      if (err) {
        console.log("find err: " + err);
      }
      if (!profession) {
        Profession.create({"name": professionName},  function(err, profession) {
          if(err) {
            console.log("create err: " + err);
          }
          category.profession = profession._id;
          category.save(function (err, categorySaved){
            if(err){
              done()
            }
            done(profession)
          });
        });
      } else {
        category.profession = profession._id;
          category.save(function(err, categorySaved){
            done(profession)
          });
      }
    });
}

function addTopicsOrContinue(area, categories, category, position, done){
  if(categories[position].topics){
    recursiveAddTopics(category, categories[position].topics, 0, function(){
      //Create template
      addTemplate(category, function(){
        //Create next category
        position = position+1;
        if(position<categories.length){
          recursiveAddCategories(area, categories, position, function(category){
            done(category);
          });
        }else{
          done(category);
        }
      });
    });
  }else{
    //Create next category
    position = position+1;
    if(position<categories.length){
      recursiveAddCategories(area, categories, position, function(category){
        done(category);
      });
    }else{
      done(category);
    }
  }
}

function recursiveAddTopics(category, topics, position, done){
  //console.log("recursiveAddTopics(" + position + ") adding " + topics[position].name + " into " + category.name);
  var topicPre = topics[position];
  Topic
  .create({name: topicPre.name}, function(err, topic){
    if(err){

    }else{
      //Add to category
      category.topics.push(topic);
      category.save(function (err, category){
        if(topics[position].questions) {
          recursiveAddQuestions(topic, topics[position].questions, 0, function(){
            //Create next topic
            position = position+1;
            if(position<topics.length){
              recursiveAddTopics(category, topics, position, function(){
                done()
              });
            }else{
              done();
            }
          });
        }else{
          //Create next topic
          position = position+1;
          if(position<topics.length){
            recursiveAddTopics(category, topics, position, function(){
              done()
            });
          }else{
            done();
          }
        }
      });
    }
  });
}

function recursiveAddQuestions(topic, questions, position, done){
  //console.log("recursiveAddQuestions(" + position + ") adding " + questions[position].name + " with topic -> " + topic.name);
  var questionPre = questions[position];
  Question
  .create({"name": questionPre.name, "topic": topic._id}, function(err, topic){
    if(err){

    }else{
      //Create next question
      var nextPosition = position+1;
      if(nextPosition<questions.length){
        recursiveAddQuestions(topic, questions, nextPosition, function(){
          done();
        });
      }else{
        done();
      }
    }
  });
}

var templateNumber = 0;

function addTemplate(category, done){
  var number = templateNumber;
  console.log("template id: " + number + " | category.name: " + category.name)
  templateNumber = templateNumber+1;
  Template
  .create({"name": "template"+category.name, "description": "description"+category.name, "active": true, "category": category._id}, function(err, template){
    if(err){
      console.log("Create template error")
      done()
    }else{
      Question
      .find()
      .where("topic").in(category.topics)
      .exec(function(err, questions){
        if(err){
          console.log("Find question error: " + err)
        }else{
          console.log("Find questions ok:   " + questions.length + " category name: " + category.name);
          template.questions = questions;
          template.save(function(err, template){
            if(err){
              console.log("Error saving template with questions");
            }
            done()  
          });
        }
      });
    }
  });
}