'use strict';

var express = require('express');
var controller = require('./area.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

//TODO auth.isAuthenticated() y filtrar por profesion

router.get('/', controller.index);
router.get('/:idArea', controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
//router.put('/:idArea', controller.update);
//router.patch('/:idArea', controller.patch);
//router.delete('/:idArea', controller.destroy);

router.get('/:idArea/categories', controller.getCategories);
router.get('/:idArea/categories/:idCategory', controller.getCategory);
router.post('/:idArea/categories', auth.hasRole('admin'), controller.addCategory);
router.delete('/:idArea/categories/:idCategory', auth.hasRole('admin'), controller.deleteCategory);

module.exports = router;