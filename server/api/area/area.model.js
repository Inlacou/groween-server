'use strict';
console.log('area.model.js');

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var TopicSchema = new Schema({
  name:         { type: String, trim: true, required: true }
  , created_at    : { type: Number }
  , updated_at    : { type: Number }
});

var CategorySchema = new Schema({
  name:       { type: String, trim: true, required: true }
    , profession:   { type: Schema.ObjectId, ref: 'Profession' } // Solo las categorias del area profesional tienen una profesion
    , topics:       [ TopicSchema ]
    , created_at    : { type: Number }
    , updated_at    : { type: Number }
});

var AreaSchema = new Schema({
    name: 			{ type: String, trim: true, required: true, unique: true }
    , hardId:   { type: Number, required: true, unique: true }
    , categories: 	[ CategorySchema ]
    , created_at    : { type: Number }
    , updated_at    : { type: Number }
});

TopicSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

CategorySchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

AreaSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

AreaSchema.post('save', function (doc) {

});

AreaSchema.pre('remove', function (next) {
  next();
});

AreaSchema.post('remove', function (doc) {

});

TopicSchema.pre('remove', function (next) {
  console.log("TopicSchema.remove.pre")
  console.log(this)
  var Question = mongoose.model('Question');
  Question
  .find({'topic': this._id})
  .populate('questions')
  .exec(function(err, questions){
    if(err){
      console.log('ERROR   on TopicSchema.remove.pre: ' + err);
    }else if(!questions || !questions.length){
      console.log("WARNING on TopicSchema.remove.pre: questions not found, maybe it didn't have any");
    }else{
      for (var i = questions.length - 1; i >= 0; i--) {
        questions[i].remove(function(err){
          if(err){
            console.log("ERROR   removing question when removing topic")
          }else{
            console.log("SUCCESS removing question when removing topic")
          }
        });
      };
    }
    next();
  });
});

CategorySchema.pre('remove', function (next) {
  console.log("CategorySchema.remove.pre")
  console.log(this)
  var topics = this.topics;
  if(!topics || !topics.length){
    console.log("WARNING on CategorySchema.remove.pre: topics not found, maybe it didn't have any");
  }else{
    for (var i = topics.length - 1; i >= 0; i--) {
      console.log(topics[i]);
      topics[i].remove(function(err){
        if(err){
          console.log("ERROR   removing topic when removing category")
        }else{
          console.log("SUCCESS removing topic when removing category")
        }
      });
    };
  }

  var Template = mongoose.model('Template');
  Template
  .find({'category': this._id})
  .exec(function(err, templates){
    if(err){
      console.log("ERROR   finding templates when removing category")
    }else if(!templates || !templates.length){
      console.log("WARNING on CategorySchema.remove.pre: templates not found, maybe it didn't have any");
    }else{
      for (var i = templates.length - 1; i >= 0; i--) {
        templates[i].remove(function(err){
          if(err){
            console.log("ERROR   removing template when removing category")
          }else{
            console.log("SUCCESS removing template when removing category")
          }
        });
      };
    }
  });

  next();
});

AreaSchema.pre('remove', function (next) {
  console.log("AreaSchema.remove.pre")
  console.log(this)
  var categories = this.categories;
  if(!categories || !categories.length){
    console.log("WARNING on AreaSchema.remove.pre: categories not found, maybe it didn't have any");
  }else{
    for (var i = categories.length - 1; i >= 0; i--) {
      console.log(categories[i]);
      categories[i].remove(function(err){
        if(err){
          console.log("ERROR   removing category when removing area")
        }else{
          console.log("SUCCESS removing category when removing area")
        }
      });
    };
  }
  next();
});

mongoose.model('Topic', TopicSchema);
mongoose.model('Category', CategorySchema);
module.exports = mongoose.model('Area', AreaSchema);
