'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var QuestionarySchema = new Schema({
  name: 				{ type: String, trim: true, required: true }
  , active: 			{ type: Boolean, default: true }
  , template: 			{ type: Schema.ObjectId, ref: 'Template' }
  , questions: 			[ { type: Schema.ObjectId, ref: 'Question' } ]
  , selfAnswer:     { type: Schema.ObjectId, ref: 'QuestionaryAnswer' }
  , questionaryAnswers: [ { type: Schema.ObjectId, ref: 'QuestionaryAnswer' } ]
  , created_at: 		{ type: Number }
  , updated_at: 		{ type: Number }
  , pushOnNewAnswer: { type: Boolean, default: true }
  //TODO virtual //www.groween.com/id //obtener el root (www.groween.com) de forma dinamica
});

QuestionarySchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

module.exports = mongoose.model('Questionary', QuestionarySchema);
