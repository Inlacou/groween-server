'use strict';

var _ = require('lodash');
var AccountType = require('./accountType.model');

// Get list of accountTypes
exports.index = function(req, res) {
  AccountType.find(function (err, accountTypes) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(accountTypes);
  });
};

// Get a single accountType
exports.show = function(req, res) {
  AccountType.findById(req.params.id, function (err, accountType) {
    if(err) { return handleError(res, err); }
    if(!accountType) { return res.status(404).json(); }
    return res.json(accountType);
  });
};

// Creates a new accountType in the DB.
exports.create = function(req, res) {
  AccountType.create(req.body, function(err, accountType) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(accountType);
  });
};

// Updates an existing accountType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  AccountType.findById(req.params.id, function (err, accountType) {
    if (err) { return handleError(res, err); }
    if(!accountType) { return res.status(404).json(); }
    var updated = _.merge(accountType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(accountType);
    });
  });
};

// Deletes a accountType from the DB.
exports.destroy = function(req, res) {
  AccountType.findById(req.params.id, function (err, accountType) {
    if(err) { return handleError(res, err); }
    if(!accountType) { return res.status(404).json(); }
    accountType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}