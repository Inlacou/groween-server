'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var NgoSchema = new Schema({
  name: 			{ type: String, trim: true, required: true }
  , description: 	{ type: String, trim: true, required: false }
  , totalDonation: 	{ type: Number, default: 0 }
  , active: 		{ type: Boolean, default: false }
  , url: 			{ type: String, trim: true, required: false }
  , image: 			{ type: Schema.ObjectId, ref: 'Image' }
  , created_at:     { type: Number }
  , updated_at:     { type: Number }
});

NgoSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

NgoSchema.pre('remove', function(next){
	if(this.totalDonation){
		next(new Error("ERROR while removing NGO: can't remove NGO with donations generated"));
	}
	next();
});

module.exports = mongoose.model('NGO', NgoSchema);
