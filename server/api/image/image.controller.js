'use strict';

var _ = require('lodash');
var image = require('./image.model');
var gm = require('../../images/gm.controller.js'),
    mkdirp = require('mkdirp');

// Get list of images
exports.index = function(req, res) {
  image.find(function (err, images) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(images);
  });
};

// Get a single image
exports.show = function(req, res) {
  if(!req.query.multiplier){
    req.query.multiplier=1;
  }
  image
  .findOne({ '_id': req.params.id })
  .exec(function (err, image) {
    if(err) {
      return handleError(res, err);
    }
    if(!image) {
      return res.status(404).json();
    }
    if(req.query.name){
      var found = false;
      // with or without simple and double quotes
      var imageName = req.query.name;
      imageName = imageName.replace(/"/g, "");
      imageName = imageName.replace(/'/g, "");
      for (var i = image.images.length - 1; i >= 0; i--) {
        console.log(image.images[i].name + ' vs ' + imageName);
        if(image.images[i].name==imageName){
          found = true;
          gm.getImageResized(image.location+image.images[i].width+"x"+image.images[i].height+"/reference."+image.images[i].extension,
                             image.images[i].extension, image.images[i].width, image.images[i].height, req.query.multiplier, function(err, extension, buffer){
            if(err){
              return res.status(500).json();
            }
            return res.type('png').end(buffer, 'binary');
          });
        }
      };
      if(!found){
        return res.status(404).json();
      }
    }else{
      gm.getImageResized(image.location, null, null, req.query.multiplier, function(err, buffer){
        if(err){
          return res.status(500).json();
        }
        return res.end(buffer)
      });
    }
  });
};

// Creates a new image in the DB.
exports.create = function(req, res) {
  console.log("image.controller.js.create")
  if (typeof req.body.images == 'string') {
    req.body.images = JSON.parse(req.body.images);
  }
  var parts = req.body.name.split(".")
  var fileName = parts[0];
  var fileExtension = parts[parts.length-1];

  var rand = (Math.random()+"").replace(".", "");

  var dir = "server/pictures/"+fileName+rand+"/";

  mkdirp(dir, function(err, made){
    if(err){
      return res.status(500).json()
    }
    gm.saveImage(dir, fileName, fileExtension, req.body.base64, req.body.images, function (err, path, name, extension){
      if(err) {
        return res.status(500).json()
      } else {
        req.body.location = path;
        req.body.extension = extension;
        for (var i = req.body.images.length - 1; i >= 0; i--) {
          req.body.images[i].location = path;
          req.body.images[i].extension = extension;
        };
        image.create(req.body, function(err, image) {
          if(err) {
            return handleError(res, err);
          }
          console.log("Image:\n\n" + image);
          return res.status(201).json(image);
        });
      }
    });
  });
};

// Updates an existing image in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  if(req.body.totalDonation) { delete req.body.totalDonation; }
  image
    .findOne({_id: req.params.id})
    .exec(function (err, image) {
    if (err) { return handleError(res, err); }
    if(!image) { return res.status(404); }
    var updated = _.merge(image, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).end(image);
    });
  });
};

// Deletes a image from the DB.
exports.destroy = function(req, res) {
  image.findById(req.params.id, function (err, image) {
    if(err) { return handleError(res, err); }
    if(!image) { return res.status(404); }
    image.remove(function(err) {
      if(err) { return res.status(409, err); }
      return res.status(204).json();
    });
  });
};

// Deletes all images from the DB.
exports.destroyAll = function(req, res) {
  image.find().remove(function(err) {
    if(err) { return res.status(409, err); }
    return res.status(204).json();
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}
