'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var authTypes = ['github', 'twitter', 'facebook', 'google'];

var UserSchema = new Schema({
  email: { type: String, lowercase: true, unique: true },
  role: {
    type: String,
    default: 'user'
  },
  profile: {
    name: String,
    lastname: String,
    birthdate: { type: Number },
    userSex: { type: Number }, // 0-Female; 1-Male; 2-Undefined
    country: String,
    pc: String,
    studyLevel: { type: Number }, // 0-Primary studies; 1-Primary studies; 2-Secundary studies 3-Superior studies
    questionaries: [ { type: Schema.ObjectId, ref: 'Questionary' } ],
    categories: [ { type: String } ],
    subscriptionType: { type: Schema.ObjectId, ref: 'SubscriptionType' },
    profession: { type: Schema.ObjectId, ref: 'Profession' }
  },
  hashedPassword: String,
  refreshToken: String,
  provider: String,
  salt: String,
  gcm: {
    registrationIds: [ { type: String, trim: true } ],
    topics: [ { type: String, trim: true } ]
  },
  facebook: {},
  twitter: {},
  google: {},
  github: {}
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
});

UserSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

/**
 * Virtuals
 */
UserSchema
    .virtual('password')
    .set(function(password) {
      this._password = password;
      this.salt = this.makeSalt();
      this.refreshToken = this.makeRefreshToken();
      this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() {
      return this._password;
    });


// Login and registration response
UserSchema
    .virtual('info')
    .get(function() {
      return {
        '_id': this._id,
        'provider': this.provider,
        'email': this.email,
        'refreshToken': this.refreshToken,
        '__v': this.__v,
        'gcm': this.gcm,
        'profile': this.profile,
        'role': this.role
      };
    });

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function() {
      return {
        '_id': this._id,
        'role': this.role
      };
    });

/**
 * Validations
 */

// Validate empty email
UserSchema
    .path('email')
    .validate(function(email) {
      if (authTypes.indexOf(this.provider) !== -1) return true;
      return email.length;
    }, 'Email cannot be blank');

// Validate empty password
UserSchema
    .path('hashedPassword')
    .validate(function(hashedPassword) {
      if (authTypes.indexOf(this.provider) !== -1) return true;
      return hashedPassword.length;
    }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
    .path('email')
    .validate(function(value, respond) {
      var self = this;
      this.constructor.findOne({email: value}, function(err, user) {
        if(err) throw err;
        if(user) {
          if(self.id === user.id) return respond(true);
          return respond(false);
        }
        respond(true);
      });
    }, 'The specified email address is already in use.');

var validatePresenceOf = function(value) {
  return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function(next) {
      if (!this.isNew) return next();

      if (!validatePresenceOf(this.hashedPassword) && authTypes.indexOf(this.provider) === -1)
        next(new Error('Invalid password'));
      else
        next();
    });

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function() {
    return crypto.randomBytes(16).toString('base64');
  },

  /**
   * Make refresh token
   *
   * @return {String}
   * @api public
   */
  makeRefreshToken: function() {
    return crypto.randomBytes(54).toString('base64');
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function(password) {
    if (!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  }
};

/*
UserSchema.statics.addQuestionaryToUser = function(idUser, idQuestionary, done){
  var User = this;
  var Questionary = mongoose.model('Questionary');
  User.findOne({ '_id': idUser }, function(errUser, user) {
    if(!errUser) {
      Questionary.findOne({ '_id': idQuestionary }, function(errQuestionary, questionary) {
        if(!errQuestionary) {
          if(user.profile.categories.indexOf(idQuestionary)!=-1) {
            //error?
          } else {
            user.profile.categories.push(questionary);
          }
          user.save(function (err) {
              if (!err) {
                done(false, user);
              } else {
                done(true, err);
              }
          });
        } else {
          done(true, errQuestionary);
        }
      });
    } else {
      done(true, errUser);
    }
  });
}

UserSchema.statics.addCategoryToUser = function(idUser, idCategory, done){
  var User = this;
  var Category = mongoose.model('Category');
  User.findOne({ '_id': idUser }, function(errUser, user) {
    if(!errUser) {
      Category.findOne({ '_id': idCategory }, function(errCategory, category) {
        if(!errCategory) {
          if(user.profile.categories.indexOf(idCategory)!=-1) {
            //error?
          } else {
            user.profile.categories.push(category);
          }
          user.save(function (err) {
              if (!err) {
                done(false, user);
              } else {
                done(true, err);
              }
          });
        } else {
          done(true, errCategory);
        }
      });
    } else {
      done(true, errUser);
    }
  });
}
*/
module.exports = mongoose.model('User', UserSchema);
