'use strict';

var _ = require('lodash');
var Question = require('./question.model');
var mongoose = require('mongoose');

// Get list of questions
exports.index = function(req, res) {
  if(req.params.idTopic){
    Question
    .find({'topic': req.params.idTopic})
    .populate('topic')
    .exec(function (err, questions) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(questions);
    });
  }else{
    Question
    .find()
    .populate('topic')
    .exec(function (err, questions) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(questions);
    });
  }
};

// Get a single question
exports.show = function(req, res) {
  Question.findById(req.params.id, function (err, question) {
    if(err) { return handleError(res, err); }
    if(!question) { return res.status(404).json(); }
    return res.json(question);
  });
};

// Creates a new question in the DB.
exports.create = function(req, res) {
  Question.create(req.body, function(err, question) {
    if(err) { 
    	return handleError(res, err); 
    }
    if(req.params.idTemplate){
    	var Template = mongoose.model('Template');
    	Template
    	.findOne({'_id': req.params.idTemplate})
    	.exec(function(err, template){
              console.log(template)
    		template.questions.push(question);
    		template.save(function(err){
    			if(err){
					    return res.status(500).json();
    			}else{
              return res.status(201).json(question);
    			}
    		});
    	});
    }else{
    	return res.status(201).json(question);
    }
  });
};

// Updates an existing question in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Question.findById(req.params.id, function (err, question) {
    if (err) { return handleError(res, err); }
    if(!question) { return res.status(404); }
    var updated = _.merge(question, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200, question);
    });
  });
};

// Deletes a question from the DB.
exports.destroy = function(req, res) {
  Question.findById(req.params.id, function (err, question) {
    if(err) { return handleError(res, err); }
    if(!question) { return res.status(404); }
    question.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500, err);
}