'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var QuestionSchema = new Schema({
  name: 	{ type: String, trim: true, required: true }
  , topic: 	{ type: Schema.ObjectId, ref: 'Topic' }
  , created_at:     { type: Number }
  , updated_at:     { type: Number }
});

QuestionSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

QuestionSchema.post('remove', function (doc) {
	console.log(doc)
	var Template = mongoose.model('Template');
	Template
	.find({'questions': doc._id})
	.exec(function(err, templates){
		if(err){
			console.log('ERROR   on question.model.js.save.post: ' + err);
		}else if(!templates || !templates.length){
			console.log("WARNING on question.model.js.save.post: templates not found, maybe it didn't have any");
		}else{
			for (var i = templates.length - 1; i >= 0; i--) {
				templates[i].active = false;
				templates[i].save();
			};
		}
	});
});

module.exports = mongoose.model('Question', QuestionSchema);
