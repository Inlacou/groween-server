'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ResourceTypeSchema = new Schema({
  name: 		{ type: String, trim: true, required: true }
  , tops: 		[ { type: Schema.ObjectId, ref: 'ReinforceContent' } ]
  , category: 	{ type: Schema.ObjectId, ref: 'Category' }
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
});

ResourceTypeSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

module.exports = mongoose.model('ResourceType', ResourceTypeSchema);