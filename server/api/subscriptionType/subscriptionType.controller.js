'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var SubscriptionType = require('./subscriptionType.model');

exports.index = function(req, res) {
  SubscriptionType
  .find()
  .exec(function(err, subscriptionTypes){
    if(err){
      return res.status(500).json();
    }
    return res.json(subscriptionTypes);    
  });
};

exports.show = function(req, res) {
  SubscriptionType
  .findOne({'_id': req.params.id})
  .exec(function(err, subscriptionType){
    if(err){
      return res.status(500).json();
    }
    if(!subscriptionType){
      return res.status(404).json();
    }
    return res.json(subscriptionType);    
  });
};

exports.create = function(req, res) {
  SubscriptionType
  .create(req.body, function(err, subscriptionType) {
    if(err) { 
      return handleError(res, err); 
    }else{
      return res.status(201).json(subscriptionType);
    }
  });
};

exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  SubscriptionType
  .findById(req.params.id, function (err, subscriptionType) {
    if (err) { return handleError(res, err); }
    if(!subscriptionType) { return res.status(404).json(); }
    var updated = _.merge(subscriptionType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(subscriptionType);
    });
  });
};

exports.destroy = function(req, res) {
  SubscriptionType
  .findById(req.params.id, function (err, subscriptionType) {
    if(err) { return handleError(res, err); }
    if(!subscriptionType) { return res.status(404).json(); }
    subscriptionType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}