'use strict';

angular.module('groweenApp')
  .controller('NgosCtrl', function ($scope, $http) {
    $http.get('api/ngos/totalDonation').success(function(data) {
      $scope.total = data.totalDonation;
    });

    $scope.oneAtTime = true;
    $http.get('/api/ngos').success(function(data) {
      $scope.ngos = data;
    });

    $scope.activeChanged = function(index) {
      var ngo = $scope.ngos[index];
      $http.put('/api/ngos/' + ngo._id, {active: ngo.active});
    };

    $scope.addNgo = function() {
      if(!$scope.newNgo || $scope.newNgo === '') {
        return;
      }
      var post = { name: $scope.newNgo };
      $http.post('/api/ngos/', post).
        success(function(data, status, headers, config) {
          $scope.newNgo = '';
          $scope.ngos.push(data);
        }).
        error(function(data, status, headers, config) {
          alert('Error creating ngo\n' + status + '\n' + data);
        });
    };

    $scope.deleteNgo = function(ngoId, index) {

      if (ngoId) {
        $http.delete('/api/ngos/' + ngoId);
        $scope.ngos.splice(index, 1);
      }
    };

  });
