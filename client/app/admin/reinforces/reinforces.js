'use strict';

angular.module('groweenApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/reinforces', {
        templateUrl: 'app/admin/reinforces/reinforces.html',
        controller: 'ReinforcesCtrl'
      });
  });
