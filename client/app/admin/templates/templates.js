'use strict';

angular.module('groweenApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/templates', {
        templateUrl: 'app/admin/templates/templates.html',
        controller: 'TemplatesCtrl'
      });
  });
