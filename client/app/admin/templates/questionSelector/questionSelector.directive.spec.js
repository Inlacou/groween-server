'use strict';

describe('Directive: questionSelector', function () {

  // load the directive's module and view
  beforeEach(module('groweenApp'));
  beforeEach(module('app/admin/templates/questionSelector/questionSelector.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<question-selector></question-selector>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the questionSelector directive');
  }));
});