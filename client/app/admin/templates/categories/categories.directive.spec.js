'use strict';

describe('Directive: categories', function () {

  // load the directive's module and view
  beforeEach(module('groweenApp'));
  beforeEach(module('app/admin/templates/categories/categories.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<categories></categories>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the categories directive');
  }));
});