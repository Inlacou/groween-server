'use strict';

angular.module('groweenApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/rate/:id', {
        templateUrl: 'app/rate/pages/home.html',
        controller: 'RateHomeCtrl'
      })
      .when('/rate/:id/form', {
        templateUrl: 'app/rate/pages/form.html',
        controller: 'RateFormCtrl'
      })
      .when('/rate/:id/last/:selectedOng/:yourDonation', {
        templateUrl: 'app/rate/pages/last.html',
        controller: 'RateLastCtrl'
      });
  });
